package Logger.logger;

import com.expertsoft.jobs.manager.JobsManagerImpl;
import com.expertsoft.logger.ConsoleLogger;
import com.expertsoft.logger.EmptyLogger;
import com.expertsoft.logger.Logger;
import com.expertsoft.logger.factory.FileLogMessageFactory;

/**
 * Hello world!
 *
 */
public class App {
	private static Logger logger;

	public static void initializeConsoleLogger() {
		logger = new ConsoleLogger();
	}
	
	public static void initializeFileLogger() {
		logger = new EmptyLogger();
		((EmptyLogger)logger).setJobsManager(new JobsManagerImpl());
		((EmptyLogger)logger).setJobsFactory(new FileLogMessageFactory("logfile.txt"));
	}

	public static void main( String[] args ) {
		initializeConsoleLogger();

		logger.log("Hello from console logger!!!");

		testLogger(logger);

		initializeFileLogger();

		testLogger(logger);
	}
	
	private static void testLogger(final Logger logger) {
		Runnable job1 = new Runnable() {

			public void run() {
				try {
					logger.log("Job1 mesage 1");
					//Thread.sleep(1000);
					
					logger.log("Job1 message 2");
					//Thread.sleep(1000);
					
					logger.log("Job1 message 3");
					//Thread.sleep(1000);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		};
		Runnable job2 = new Runnable() {

			public void run() {
				try {
					logger.log("Job2 mesage 1");
					//Thread.sleep(500);
					
					logger.log("Job2 message 2");
					//Thread.sleep(500);
					
					logger.log("Job2 message 3");
					//Thread.sleep(500);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		};
		Thread t1 = new Thread(job1);
		Thread t2 = new Thread(job2);
		
		t1.start();
		t2.start();
	}
}
