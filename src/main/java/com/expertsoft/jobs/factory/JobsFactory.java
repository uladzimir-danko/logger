package com.expertsoft.jobs.factory;

import com.expertsoft.jobs.job.Job;

public interface JobsFactory {

	public Job createJob(Object... objects);

}
