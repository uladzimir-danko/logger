package com.expertsoft.jobs.manager.performers;

import java.util.Queue;

import com.expertsoft.jobs.job.Job;

public class JobsPerformerImpl implements Runnable {

	private volatile Queue<Job> jobsQueue;

	public void run() {
		while ( !Thread.interrupted() ) {
			synchronized (jobsQueue) {
				if ( !jobsQueue.isEmpty() ) {
					Thread jobThread = new Thread(jobsQueue.poll());
					jobThread.start();
				} else {
					try {
						jobsQueue.wait();
					} catch (InterruptedException e) {
						System.out.println(e);
					}
				}
			}
		}
	}

	public Queue<Job> getJobsQueue() {
		return jobsQueue;
	}

	public void setJobsQueue(Queue<Job> jobsQueue) {
		this.jobsQueue = jobsQueue;
	}

	public Job nextJob() {
		// TODO Auto-generated method stub
		return null;
	}

}
