package com.expertsoft.jobs.manager.performers;

import java.util.Queue;

import com.expertsoft.jobs.job.Job;

public class SimpleJobsPerformer implements Runnable {

	private volatile Queue<Job> jobsQueue;
	
	public SimpleJobsPerformer(Queue<Job> pJobsQueue) {
		this.jobsQueue = pJobsQueue;
	}

	public void run() {
		while ( !Thread.interrupted() ) {
			synchronized (jobsQueue) {
				Job job = nextJob();
				if ( job != null ) {
					job.run();
				} else {
					try {
						jobsQueue.wait();
					} catch (InterruptedException e) {
						System.out.println(e);
					}
				}
			}
		}
	}

	private Job nextJob() {
		if (!jobsQueue.isEmpty()) {
			return jobsQueue.poll();
		}
		return null;
	}
}
