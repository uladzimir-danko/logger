package com.expertsoft.jobs.manager;

import java.util.List;

import com.expertsoft.jobs.job.Job;

public interface JobsManager {

	public void addJob(Job job);

	public void addJobsList(List<Job> jobs);

	public void runJobs();

	public void stopJobsRunning();

}
