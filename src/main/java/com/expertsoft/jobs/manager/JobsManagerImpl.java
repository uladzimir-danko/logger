package com.expertsoft.jobs.manager;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import com.expertsoft.jobs.factory.JobsFactory;
import com.expertsoft.jobs.job.Job;
import com.expertsoft.jobs.manager.performers.SimpleJobsPerformer;

public class JobsManagerImpl implements JobsManager {
	private volatile Queue<Job> jobsQueue;
	private JobsFactory jobsFactory;
	private volatile Thread jobsPerformer;

	public JobsManagerImpl() {
		jobsQueue = new LinkedList<Job>();
		runJobs();
	}

	public void addJob(Job job) {
		synchronized (jobsQueue) {
			jobsQueue.add(job);
			jobsQueue.notifyAll();
		}
	}

	public void addJobsList(List<Job> jobs) {
		synchronized (jobsQueue) {
			jobsQueue.addAll(jobs);
			jobsQueue.notifyAll();
		}
	}

	public synchronized void runJobs() {
		if (jobsPerformer != null) {
			if (!jobsPerformer.isAlive()) {
				jobsPerformer = new Thread(new SimpleJobsPerformer(jobsQueue));
				jobsPerformer.setDaemon(true);
				jobsPerformer.start();
			} else {
				System.out.println("Jobs are starting!!!");
			}

		} else {
			jobsPerformer = new Thread(new SimpleJobsPerformer(jobsQueue));
			jobsPerformer.start();
		}
	}

	public void stopJobsRunning() {
		synchronized (jobsPerformer) {
			jobsPerformer.interrupt();
		}
	}

	public JobsFactory getJobsFactory() {
		return jobsFactory;
	}

	public void setJobsFactory(JobsFactory jobsFactory) {
		this.jobsFactory = jobsFactory;
	}

}
