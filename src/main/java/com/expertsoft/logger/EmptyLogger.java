package com.expertsoft.logger;

import com.expertsoft.jobs.factory.JobsFactory;
import com.expertsoft.jobs.manager.JobsManager;

public class EmptyLogger implements Logger {
	private JobsManager jobsManager;
	private JobsFactory jobsFactory;

	public void log(String message) {
		jobsManager.addJob(jobsFactory.createJob(message));
	}

	public JobsManager getJobsManager() {
		return jobsManager;
	}

	public void setJobsManager(JobsManager jobsManager) {
		this.jobsManager = jobsManager;
	}

	public JobsFactory getJobsFactory() {
		return jobsFactory;
	}

	public void setJobsFactory(JobsFactory jobsFactory) {
		this.jobsFactory = jobsFactory;
	}

}
