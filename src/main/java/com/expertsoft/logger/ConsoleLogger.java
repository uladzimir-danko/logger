package com.expertsoft.logger;

import com.expertsoft.jobs.factory.JobsFactory;
import com.expertsoft.jobs.manager.JobsManager;
import com.expertsoft.jobs.manager.JobsManagerImpl;
import com.expertsoft.logger.factory.ConsoleLogMessageFactory;

public class ConsoleLogger implements Logger {
	private JobsManager jobsManager;
	private JobsFactory jobsFactory;

	public ConsoleLogger() {
		jobsManager = new JobsManagerImpl();
		jobsFactory = new ConsoleLogMessageFactory();
	}

	public void log(String message) {
		jobsManager.addJob(jobsFactory.createJob(message));
	}

	public JobsManager getJobsManager() {
		return jobsManager;
	}

	public void setJobsManager(JobsManager jobsManager) {
		this.jobsManager = jobsManager;
	}

	public JobsFactory getJobsFactory() {
		return jobsFactory;
	}

	public void setJobsFactory(JobsFactory jobsFactory) {
		this.jobsFactory = jobsFactory;
	}

}
