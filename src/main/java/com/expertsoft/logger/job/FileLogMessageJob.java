package com.expertsoft.logger.job;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.expertsoft.jobs.job.Job;

public class FileLogMessageJob implements Job {
	private String message;
	private Path file;

	public FileLogMessageJob(String pMessage, Path path) {
		message = pMessage;
		file = path;
	}

	public void run() {
		List<String> messageList = new ArrayList<String>();
		messageList.add(new Date() + ": " + message);

		try {
			Files.write(file, messageList, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
