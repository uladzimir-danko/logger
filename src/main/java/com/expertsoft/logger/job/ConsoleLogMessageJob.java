package com.expertsoft.logger.job;

import java.util.Date;

import com.expertsoft.jobs.job.Job;

public class ConsoleLogMessageJob implements Job {

	private String message;
	
	public ConsoleLogMessageJob(String pMessage) {
		message = pMessage;
	}

	public void run() {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(new Date() + ": " + message);
	}

}
