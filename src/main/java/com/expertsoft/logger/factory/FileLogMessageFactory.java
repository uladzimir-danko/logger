package com.expertsoft.logger.factory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.expertsoft.jobs.factory.JobsFactory;
import com.expertsoft.jobs.job.Job;
import com.expertsoft.logger.job.FileLogMessageJob;

public class FileLogMessageFactory implements JobsFactory {
	private Path file;

	public FileLogMessageFactory(String filePath) {
		File yourFile = new File(filePath);
		try {
			yourFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}

		file = Paths.get(filePath);
	}

	public Job createJob(Object... objects) {
		for (Object param : objects) {
			if (param instanceof String) {
				return new FileLogMessageJob((String) param, file);
			}
		}

		return null;
	}
}
