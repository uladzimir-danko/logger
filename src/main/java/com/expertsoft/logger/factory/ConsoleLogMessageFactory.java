package com.expertsoft.logger.factory;

import com.expertsoft.jobs.factory.JobsFactory;
import com.expertsoft.jobs.job.Job;
import com.expertsoft.logger.job.ConsoleLogMessageJob;

public class ConsoleLogMessageFactory implements JobsFactory {

	public Job createJob(Object... objects) {
		for (Object param : objects) {
			if (param instanceof String) {
				return new ConsoleLogMessageJob((String) param);
			}
		}

		return null;
	}

}
